import styles from '../styles/ContactCode.module.css';

const ContactCode = () => {
  return (
    <div className={styles.code}>
      <p className={styles.line}>
        <span className={styles.className}>.socials</span> &#123;
      </p>
      <p className={styles.line}>
        &nbsp;&nbsp;&nbsp;email:{' '}
        <a
          href="mailto:charlize.debeer@yandex.com"
          target="_blank"
          rel="noopener"
        >
         charlize.debeer@yandex.ru
        </a>
        ;
      </p>
      <p className={styles.line}>
        &nbsp;&nbsp;&nbsp;github:{' '}
        <a href="https://github.com/chardebeer" target="_blank" rel="noopener">
          chardebeer
        </a>
        ;
      </p>
          <p className={styles.line}>
              &nbsp;&nbsp;&nbsp;gitlab:{' '}
              <a href="https://gitlab.com/charlizedebeer" target="_blank" rel="noopener">
                  charlizedebeer </a>
              ;
      </p>
      <p className={styles.line}>
        &nbsp;&nbsp;&nbsp;linkedin:{' '}
        <a
          href="https://www.linkedin.com/in/charlizedebeer/"
          target="_blank"
          rel="noopener"
        >
          charlizedebeer
        </a>
        ;
          </p>
      <p className={styles.line}>&#125;</p>
        <p className={styles.line}>
            &nbsp;&nbsp;&nbsp;Behance:{' '}
            <a
                href="https://www.behance.net/charlizedebeer/"
                target="_blank"
                rel="noopener"
            >
                charlizedebeer
            </a>
            ;
        </p>
        <p className={styles.line}>&#125;</p>
    </div>
  );
};

export default ContactCode;

import styled from "styled-components";


const Heading = styled.div`
  background-color: #cca7bc;
  font-family: "Source Code Pro", sans-serif;
  font-size: 1.75rem;
  text-underline: none;
  color: white;
`

const ExperienceTitle = styled.div`
  font-family: "Source Code Pro", sans-serif;
  font-size: 1.6rem;
  color: #9e87ce;
  text-decoration: underline;
  line-height: 2rem;
`

const CompanyHeading = styled.div`
  font-family: "Source Code Pro", sans-serif;
  font-size: 1rem;
  color: rgba(213, 203, 232, 0.88);
  font-weight: bold;
  line-height: 2rem;
`

const DateHeading = styled.text`
  font-size: 1rem;
  font-weight: bold;
  color: rgba(225, 225, 225, 0.42);

`
const AboutMeList = styled.ul`
  line-height: 1.5rem;
  color: white;
  list-style-type: none;
  li{
    font-size: 1rem;
list-style-type: square;
  }
`
const AboutPage = () => {
  return (
    <div>
        <Heading>   A Little Bit About Me… </Heading>
        <br/><br/>
             <AboutMeList>
                 <li> 👋  Hi,  I’m Charlize</li>

                 <li> ✨  I’m interested in creating collaborative digital spaces focused around sustainability, innovation & open collaboration. I'm passionate about quantum physics, particle physics, metaphysics and theoretical physics and hope to someday be able to incorporate what I currently do in these fields.</li>
                 <li> 💖  I’m always open  to collaborating on any projects that are focused on physics, metaphysical sciences, anthropology, astronomy, law, accounting, and psychology.</li>
                 <li> 💬  How to reach me: charlize.debeer@yandex.ru</li>
             </AboutMeList>

        <br/><br/>
            <blockquote>
                I am a passionate frontend developer & engineer with fullstack capabilities with extensive experience in blending the art of
                design with the skill of programming to create beautiful, immersive and engaging user experiences through efficient development,
                and proactive feature optimization. I have worked with a diverse range of technologies and frameworks. I am super passionate about
                aesthetics and UI design.

            </blockquote>
        <br/><br/>

        <Heading> Professional Experience</Heading>
        <br/><br/>

        <ExperienceTitle> Full-Stack Developer </ExperienceTitle>


        <CompanyHeading> Freelance</CompanyHeading>
<DateHeading>Jan 2017 - Present (4 years 8 months +)
</DateHeading>


        <AboutMeList>

            <li>Development of progressive web applications</li>

                <li>ReactJS & React Native Development</li>

                    <li>Front-End Development with AngularJS & ReactJS</li>

                        <li>API & Microservices Design & Development</li>

                            <li>MEAN & MERN Stack Development</li>

                                <li>CI/CD & Atlassian Consulting / Professional Services</li>

                                    <li>Solutions Architecture & Project Management</li>

                                        <li>Integrated Application Development for SalesForce, Shopify, SAP &
                                            Slack</li>

                                            <li>Headless WordPress Development (Primarily E-Commerce)
                                            </li>
        </AboutMeList>
        <br/><br/>

        <ExperienceTitle> Full-Stack Web Developer</ExperienceTitle>


        <CompanyHeading> Innovating Digital - Cape Town</CompanyHeading>
        <DateHeading>Jan 2020 - Jun 2021 (1 year 6 months)</DateHeading>


        <AboutMeList>

            <li>Build websites using WordPress.</li>

                <li>Prepare website proposals to present to clients.</li>

                    <li>Provide technical support to clients.</li>

                        <li>Write PWA's using HTML, CSS, and JavaScript utilizing various JS Libraries</li>

                            <li>Serverless PWA Development</li>

                                <li>Web Framework Development using C++, Java, Python, and more.</li>

                                    <li>Salesforce & SAP Application Development</li>

                                        <li>Salesforce Cloud Applications Development</li>

                                            <li>Use of Git Version Control</li>

                                                <li>Design new features for existing websites.</li>

                                                    <li>Act as the company expert in creating PPC and Google
                                                        AdWords campaigns to be integrated with new website
                                                        builds.</li>

                                                        <li>Build custom themes using React to meet clients'
                                                            requirements.</li>

                                                            <li>Manage a user guide to help clients understand
                                                                site features and management of website to increase
                                                                visitor satisfaction.</li>

                                                                <li>Work on a new mobile development project to
                                                                    expand the company into mobile website development
                                                                    services.
                                                                </li>
        </AboutMeList>
        <br/><br/>
        <ExperienceTitle> Frontend Engineer & Managing Director</ExperienceTitle>


        <CompanyHeading>Webitude Digital</CompanyHeading>
        <DateHeading>Jan 2017 - Sep 2020 (3 years 9 months)</DateHeading>


        <AboutMeList>

            <li> Designing & Developing Applications and Websites</li>

                <li> GTK GUI Design</li>

                    <li> Enterprise Desktop Software Development & Design</li>

                        <li>Salesforce Developer & Solution Architecture</li>

                            <li>Currating and Implementing Digital Strategiest</li>

                                <li>Content Curation, and Graphic Design</li>

                                    <li>Project Management and Administration
                                    </li>
        </AboutMeList>
        <br/><br/>
        <ExperienceTitle> Front-End E-Commerce Developer</ExperienceTitle>


        <CompanyHeading> Omnix Holdings - Affiliated with Webitude Digital
</CompanyHeading>


        <AboutMeList>

            <li> Creating homepage assets for both desktop & mobile experiences.</li>

                <li> Developing site content and graphics</li>

                    <li> Website Development and Administration</li>

                        <li>Salesforce Development with Vlocity</li>

                            <li> ERP and CRM Development and Administration</li>

                                <li> Logistics Administration</li>

                                    <li>Provide technical support to clients.</li>

                                        <li>Write applications and add-ons in PHP, C++, Swift, Python and
                                            Java</li>

                                            <li>Use of Git Version Control</li>

                                                <li>Design, Develop and implement new features </li>

                                                    <li>Customize and develop web applications and company
                                                        systems</li>

                                                        <li>Work on a mobile development projects for employee
                                                            communication management
                                                        </li>
        </AboutMeList>
        <br/><br/>

        <ExperienceTitle> Website Developer & Designer, SEO, Site Maintenance, Content Creation</ExperienceTitle>


        <CompanyHeading> Freelance | Shopify Partners</CompanyHeading>



        <AboutMeList>

            <li>Shopify Development</li>

                <li>Customer Solutions Architecture</li>

                    <li>Salesforce integrations and development</li>
        </AboutMeList>
        <br/><br/>

        <ExperienceTitle> Part-Time SAP Intern & Accounting Assistant</ExperienceTitle>


    <CompanyHeading>Imperial</CompanyHeading>
        <DateHeading> Jan 2017 - Dec 2018 (2 years)</DateHeading>
        <AboutMeList>

            <li> Performed administrative tasks, including filing, reporting, etc.</li>

                <li>Prepared invoices, expense reports, and payment memos.</li>

                    <li>Compiled and analyzed company documentation for accuracy.</li>

                        <li> Performed data processing in MS Excel.</li>

                            <li> Salesforce Development & Integration</li>

                                <li> SAP development & Training</li>

                                    <li> SAP Integrations>
                                    </li>
        </AboutMeList>
        <br/><br/>

        <ExperienceTitle> E-Commerce Manager & Shopify Developer</ExperienceTitle>

       <CompanyHeading>Maxabella</CompanyHeading>
        <DateHeading>Dec 2015 - Jun 2016 (7 months) </DateHeading>
        <AboutMeList>
            <li>Manage all aspects of E-commerce and oversee day to day operations.</li>
                <li>Developing site content and graphics</li>
                    <li>Currating and Implementing Marketing Content</li>
                        <li>Website Administration and Customer Care</li>
                            <li>Intern ERP and CRM Development</li>
                                <li>Salesforce Development and Integration Training with Vlocity</li>
        </AboutMeList>
    </div>
  );
};

export async function getStaticProps() {
  return {
    props: { title: "About" },
  };
}

export default AboutPage;

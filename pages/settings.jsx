import ThemeInfo from "../components/ThemeInfo";
import styles from "../styles/SettingsPage.module.css";

const SettingsPage = () => {
  return (
    <>
      <h2>Manage Themes</h2>
      <div className={styles.container}>
        <ThemeInfo
          name="Dracula"
          icon="/dracula.png"
          publisher="Dracula Theme"
          theme="dracula"
          description="Official Dracula Theme. A dark theme for many editors, shells, and more."
        /> 
        <ThemeInfo
          name="Nord"
          icon="/nord.png"
          publisher="arcticicestudio"
          theme="nord"
          description="An arctic, north-bluish clean and elegant Visual Studio Code theme."
        />
      </div>
    </>

  );
};


export async function getStaticProps() {
  return {
    props: { title: "Settings" },
  };
}


export default SettingsPage;
